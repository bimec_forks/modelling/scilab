# Chinese (Simplified) translation for scilab
# Copyright (c) 2016 Rosetta Contributors and Canonical Ltd 2016
# This file is distributed under the same license as the scilab package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: scilab\n"
"Report-Msgid-Bugs-To: <localization@lists.scilab.org>\n"
"POT-Creation-Date: 2013-04-16 17:44+0100\n"
"PO-Revision-Date: 2020-08-15 15:01+0000\n"
"Last-Translator: GuoZhiqiang <763199198@qq.com>\n"
"Language-Team: Chinese (Simplified) <zh_CN@li.org>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Launchpad (build 1b66c075b8638845e61f40eb9036fabeaa01f591)\n"

#, c-format
msgid "%s: Wrong number of input arguments: %d expected.\n"
msgstr "%s：输入参数的数目错误，应该为%d个。\n"

#, c-format
msgid "%s: Wrong type for input argument #%d: A matrix of strings expected.\n"
msgstr "%s：第#%d个输入参数类型错误：应该是一个字符串矩阵。\n"

#, c-format
msgid "%s: Wrong type for input argument #%d: A scalar string expected.\n"
msgstr "%s： 第#%d输入参数类型错误：应该为一个字符串。\n"

#, c-format
msgid ""
"%s: Wrong type for input argument #%d: A two-columns string matrix "
"expected.\n"
msgstr "%s：第#%d个输入参数类型错误：应该是一个列维度为2的字符串矩阵。\n"

#, c-format
msgid "%s: Wrong number of input arguments: %d or %d expected.\n"
msgstr "%s：输入参数的数目错误：应该为%d个或%d个。\n"

#, c-format
msgid "%s: No active coverage: can't write.\n"
msgstr "%s：测试模块未激活：无法将数据导出。\n"

#, c-format
msgid "%s: Wrong value for input argument #%d: html output only.\n"
msgstr "%s：第#%d个输入参数的值错误：只能导出html文件。\n"

#, c-format
msgid "%s: profile is disabled.\n"
msgstr "%s：profile功能已被禁用。\n"

#, c-format
msgid "%s: Wrong type for input argument #%d: A macro or library expected.\n"
msgstr ""
"%s：第#%d个输入参数类型错误：应该是一个宏文件(macro)或者是库文件(library)。\n"

#, c-format
msgid "%s: No more memory.\n"
msgstr "%s：内存不足。\n"

#, c-format
msgid "%s: Wrong number of output arguments: %d expected.\n"
msgstr "%s：输出参数数目错误：应该为%d个。\n"
